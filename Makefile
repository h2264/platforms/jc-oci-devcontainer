# include base makefile
include .make/base.mk

# include OCI support
include .make/oci.mk

# include repo specific targets
-include RepoTargets.mk

# include workstation specific targets
-include WorkstationTargets.mk